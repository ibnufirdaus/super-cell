﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

	public GameManager gameManager;
	private float dampTime, zoomSpeed;
	private Camera cam;
	private Vector3 moveVelocity = Vector3.zero;
	private Vector3 desiredPosition = Vector3.zero;

	// Use this for initialization
	void Start ()
	{
		cam = GetComponent<Camera>();
		dampTime = .15f;
		zoomSpeed = 10f;
	}

	void FixedUpdate()
	{
		List<Transform> playerPos = gameManager.playerPos;
		Move(playerPos);
		Zoom(playerPos);
	}

	void Move(List<Transform> playerPos)
	{
		Vector3 target = new Vector3();
		int amount = 0;

		foreach (Transform t in playerPos)
		{
			if (t != null && t.gameObject.activeSelf)
			{
				target = target + t.position;
				amount++;
			}
		}

		if (amount > 0)
		{
			target /= amount;
			desiredPosition = target;
		}
		target = new Vector3(target.x, target.y + 17.5f, transform.position.z);

		transform.position = 
			Vector3.SmoothDamp(transform.position, target, ref moveVelocity, dampTime);
	}

	void Zoom(List<Transform> playerPos)
	{
		Vector3 desiredLocalPos = transform.InverseTransformPoint(desiredPosition);
		float size = 0f;
		
		foreach (Transform t in playerPos)
		{
			if (t != null && t.gameObject.activeSelf)
			{
				Vector3 targetLocalPos = transform.InverseTransformPoint(t.position);
				Vector3 desiredPosToTarget = targetLocalPos - desiredLocalPos;

				size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.y));
				size = Mathf.Max(size, Mathf.Abs(desiredPosToTarget.x) / cam.aspect);
			}
		}

		size += 30f;

		size = Mathf.Max(size, 30f);

		cam.orthographicSize = Mathf.SmoothDamp(cam.orthographicSize, size, ref zoomSpeed, dampTime);

	}
}
