﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPowerup : Powerup
{
	public float healAmount = 50f;

	protected override void Activate()
	{
		base.Activate();
		GameObject go = Instantiate(pickedUpInfo, transform.position, Quaternion.identity);
		go.GetComponent<FloatingText>().SetText("Healed !");
		pc.Damage(-healAmount);
		Destroy(gameObject);
	}
}
