﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EtherealPowerup : Powerup {
	public float duration = 3f;
	public int blockAmount = 5;
	
	protected override void Activate()
	{
		base.Activate();
		GameObject go = Instantiate(pickedUpInfo, transform.position, Quaternion.identity);
		go.GetComponent<FloatingText>().SetText("Ethereal Activated !");
		StartCoroutine("Buff");
	}

	IEnumerator Buff()
	{
		pc.bulletBlockCount += blockAmount;
		yield return new WaitForSeconds(duration);
		pc.bulletBlockCount = 0;
		Destroy(gameObject);
	}
	
}
