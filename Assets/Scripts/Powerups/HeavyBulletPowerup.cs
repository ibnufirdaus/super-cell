﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavyBulletPowerup : Powerup
{

	public int heavyBulletAmount = 5;

	protected override void Activate()
	{
		base.Activate();
		GameObject go = Instantiate(pickedUpInfo, transform.position, Quaternion.identity);
		go.GetComponent<FloatingText>().SetText("Double Damage !");
		pc.heavyBulletCount += heavyBulletAmount;
		pc.hasHeavyBullet = true;
		Destroy(gameObject);
	}
}
