﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurstPowerup : Powerup
{
	public float duration = 3f;
	
	protected override void Activate()
	{
		base.Activate();
		GameObject go = Instantiate(pickedUpInfo, transform.position, Quaternion.identity);
		go.GetComponent<FloatingText>().SetText("Burst Power Up !");
		StartCoroutine("Buff");
	}

	IEnumerator Buff()
	{
		pc.burstStamina = true;
		yield return new WaitForSeconds(duration);
		pc.burstStamina = false;
		Destroy(gameObject);
	}
}
