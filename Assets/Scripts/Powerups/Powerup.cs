﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Powerup : MonoBehaviour
{

	public bool isPicked;
	public GameObject pickedUpInfo;
	protected PlayerController pc;

	// Use this for initialization
	void Start ()
	{
		isPicked = false;
	}
	
	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player" && !isPicked)
		{
			isPicked = true;
			GetComponent<SpriteRenderer>().enabled = false;
			pc = other.GetComponent<PlayerController>();
			GameObject go = Instantiate(pickedUpInfo, transform.position, Quaternion.identity);
			go.GetComponent<FloatingText>();
			Activate();
		}
	}

	protected virtual void Activate()
	{}
}
