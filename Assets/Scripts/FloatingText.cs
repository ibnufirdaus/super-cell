﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingText : MonoBehaviour
{
    public float fadeUpTime = 1.5f;
    private float duration;
    public TextMesh textMesh;

    private void Start()
    {
        duration = fadeUpTime;
    }

    private void Update()
    {
        if (duration >= 0f)
        {
            duration -= Time.deltaTime;
            transform.SetPositionAndRotation(new Vector3(transform.position.x,
                transform.position.y + (5 * Time.deltaTime),transform.position.z), Quaternion.identity);
            textMesh.color = new Color(textMesh.color.r,textMesh.color.r,textMesh.color.r,
                Mathf.Lerp(0f,1f,duration/fadeUpTime));
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void SetText(string txt)
    {
        textMesh.text = txt;
    }
}
