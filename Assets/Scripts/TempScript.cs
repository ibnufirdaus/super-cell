﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector2 parentPos = transform.parent.transform.position;
		
		transform.rotation = Quaternion.identity;
		transform.position = new Vector2(parentPos.x,parentPos.y + 1f);
	}
}
