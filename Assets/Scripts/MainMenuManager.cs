﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuManager : MonoBehaviour
{
    public GameObject selectionPointer;

    public Text[] selections;

    public int choice;

    public int availableChoice;
    public AudioSource changeSelectionAudio, mainMenuAudioSource;

    public float choiceDelay;
    // Start is called before the first frame update
    void Start()
    {
        choice = 0;
        choiceDelay = 0f;
        availableChoice = selections.Length;
        StartCoroutine("FadeInAudio");
    }

    // Update is called once per frame
    void Update()
    {
        if (choiceDelay <= 0f)
        {
            float vertValue = Input.GetAxis("Vertical");
            if (vertValue > 0f) // go up
            {
                if (choice == 0)
                {
                    choice = availableChoice - 1;
                }
                else
                {
                    choice--;
                }
            }
            else if (vertValue < 0f)
            {
                if (choice == availableChoice - 1)
                {
                    choice = 0;
                }
                else
                {
                    choice++;
                }
            }

            if (vertValue != 0)
            {
                Debug.Log("----------------------------------------");
                Debug.Log("Choice changed to: " + choice + " | vertValue : " + vertValue);
                changeSelectionAudio.Play();
                float newYPos = selections[choice].transform.position.y -10;
                float XPos = selectionPointer.transform.position.x;
                selectionPointer.transform.SetPositionAndRotation(new Vector3(XPos, newYPos), Quaternion.identity);
                choiceDelay = .35f;
            }
        }
        else
        {
            choiceDelay = Mathf.Clamp(choiceDelay - Time.deltaTime, 0f, 1f);
        }

        if (Input.GetButtonDown("Confirm"))
        {
            Debug.Log("Choice : " + choice + " selected!");
            if (choice == 0)
            {
                SceneManager.LoadScene(1);
            }else if (choice == 1)
            {
                SceneManager.LoadScene(2);
            }else if (choice == 2)
            {
                Application.Quit();
            }
        }
    }
    
    IEnumerator FadeInAudio()
    {
        float fadingTime = 0f;
        while (fadingTime <= 1f)
        {
            mainMenuAudioSource.volume = Mathf.Lerp(0f, 1f, fadingTime);
            yield return new WaitForSeconds(Time.deltaTime);
            fadingTime += Time.deltaTime;
        }
    }
}
