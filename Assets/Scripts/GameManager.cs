﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{

	public List<Transform> playerPos;
	public Transform[] playerSpawnPositions, powerupSpawnPositions;
	public GameObject[] players,powerups;
	public Canvas[] playersUI;
	public PlayerSelection[] playerSelection;
	public Sprite[] playerChars;
	private Color[] playersColor;
	public Text countdownText;
	public CameraController camControl;
	public AudioSource audioSource;
	public AudioClip[] audioClips;
	
	[SerializeField] private int index;
	[SerializeField] private float powerupsSpawnDelay;
	private bool countdownStarted, matchStarted;
	private bool[] hasSpawned;
	private int[] playerHeroChoice, playerHeroId, playerSpawn;
	private int playerSpawnPoint;
	[SerializeField] private GameObject activePowerups; 
	// 396DB4 C32828 D4C941 85C535 9A24B2
	// Use this for initialization
	void Start ()
	{
		audioSource.clip = audioClips[0];
		audioSource.Play();
		StartCoroutine("FadeInAudio");
		
		index = 0;
		hasSpawned = new bool[5];
		playerHeroChoice = new int[5];
		playerHeroId = new int[5];
		playerSpawn = new int[5];
		powerupsSpawnDelay = 10f;
		playerSpawnPoint = 0;
		
		playersColor = new Color[5];
		playersColor[0] = new Color(0.2235294f,0.427451f,0.7058824f);
		playersColor[1] = new Color(0.7647059f,0.1568628f,0.1568628f);
		playersColor[2] = new Color(0.8552446f,0.8584906f,0.3199092f);
		playersColor[3] = new Color(0.5215687f,0.772549f,0.2078431f);
		playersColor[4] = new Color(0.6039216f,0.1411765f,0.6980392f);
		
		StartCoroutine("SpawnPowerups");
	}
	
	// Update is called once per frame
	void Update () {
		
		if (index < 4)
		{
			if (Input.GetButtonDown("j1Start") && !hasSpawned[0] && !matchStarted)
			{
				hasSpawned[0] = true;
				playerHeroId[0] = ++index;
				playerSpawn[0] = playerSpawnPoint++;
				
				playerSelection[playerSpawn[0]].ChangePointerColor(playersColor[playerSpawn[0]]);
				playerSelection[playerSpawn[0]].gameObject.SetActive(true);
				
				Debug.Log("Player 1 Joins!");
			}
			else if (Input.GetButtonDown("j2Start") && !hasSpawned[1] && !matchStarted)
			{
				hasSpawned[1] = true;
				playerHeroId[1] = ++index;
				playerSpawn[1] = playerSpawnPoint++;
				
				playerSelection[playerSpawn[1]].ChangePointerColor(playersColor[playerSpawn[1]]);
				playerSelection[playerSpawn[1]].gameObject.SetActive(true);
				
				Debug.Log("Player 2 Joins!");
			}
			else if (Input.GetButtonDown("j3Start") && !hasSpawned[2] && !matchStarted)
			{
				hasSpawned[2] = true;
				playerHeroId[2] = ++index;
				playerSpawn[2] = playerSpawnPoint++;
				
				playerSelection[playerSpawn[2]].ChangePointerColor(playersColor[playerSpawn[2]]);
				playerSelection[playerSpawn[2]].gameObject.SetActive(true);
				
				Debug.Log("Player 3 Joins!");
			}
			else if (Input.GetButtonDown("j4Start") && !hasSpawned[3] && !matchStarted)
			{
				hasSpawned[3] = true;
				playerHeroId[3] = ++index;
				playerSpawn[3] = playerSpawnPoint++;
				
				playerSelection[playerSpawn[3]].ChangePointerColor(playersColor[playerSpawn[3]]);
				playerSelection[playerSpawn[3]].gameObject.SetActive(true);
				
				Debug.Log("Player 4 Joins!");
			}
			else if (Input.GetButtonDown("j5Start") && !hasSpawned[4] && !matchStarted)
			{
				hasSpawned[4] = true;
				playerHeroId[4] = ++index;
				playerSpawn[4] = playerSpawnPoint++;
				
				playerSelection[playerSpawn[4]].ChangePointerColor(playersColor[playerSpawn[4]]);
				playerSelection[playerSpawn[4]].gameObject.SetActive(true);
				
				Debug.Log("Player 5 Joins!");
			}

			if (index >= 2 && !countdownStarted)
			{
				countdownStarted = true;
				StartCoroutine("StartCountdown");
			}
		}
		
		
		if (Input.GetButtonDown("j1Item"))
		{
			playerHeroChoice[0] = 0;
			playerSelection[playerSpawn[0]].ChangeSelectedChar(playerChars[0]);
		}
		else if (Input.GetButtonDown("j2Item"))
		{
			playerHeroChoice[1] = 0;
			playerSelection[playerSpawn[1]].ChangeSelectedChar(playerChars[0]);
		}
		else if (Input.GetButtonDown("j3Item"))
		{
			playerHeroChoice[2] = 0;
			playerSelection[playerSpawn[2]].ChangeSelectedChar(playerChars[0]);
		}
		else if (Input.GetButtonDown("j4Item"))
		{
			playerHeroChoice[3] = 0;
			playerSelection[playerSpawn[3]].ChangeSelectedChar(playerChars[0]);
		}
		else if (Input.GetButtonDown("j5Item"))
		{
			playerHeroChoice[4] = 0;
			playerSelection[playerSpawn[4]].ChangeSelectedChar(playerChars[0]);
		}
		
		
		if (Input.GetButtonDown("j1Ability2"))
		{
			playerHeroChoice[0] = 1;
			playerSelection[playerSpawn[0]].ChangeSelectedChar(playerChars[1]);
		}
		else if (Input.GetButtonDown("j2Ability2"))
		{
			playerHeroChoice[1] = 1;
			playerSelection[playerSpawn[1]].ChangeSelectedChar(playerChars[1]);
		}
		else if (Input.GetButtonDown("j3Ability2"))
		{
			playerHeroChoice[2] = 1;
			playerSelection[playerSpawn[2]].ChangeSelectedChar(playerChars[1]);
		}
		else if (Input.GetButtonDown("j4Ability2"))
		{
			playerHeroChoice[3] = 1;
			playerSelection[playerSpawn[3]].ChangeSelectedChar(playerChars[1]);
		}
		else if (Input.GetButtonDown("j5Ability2"))
		{
			playerHeroChoice[4] = 1;
			playerSelection[playerSpawn[4]].ChangeSelectedChar(playerChars[1]);
		}
		
		
		if (Input.GetButtonDown("j1Fire"))
		{
			playerHeroChoice[0] = 2;
			playerSelection[playerSpawn[0]].ChangeSelectedChar(playerChars[2]);
		}
		else if (Input.GetButtonDown("j2Fire"))
		{
			playerHeroChoice[1] = 2;
			playerSelection[playerSpawn[1]].ChangeSelectedChar(playerChars[2]);
		}
		else if (Input.GetButtonDown("j3Fire"))
		{
			playerHeroChoice[2] = 2;
			playerSelection[playerSpawn[2]].ChangeSelectedChar(playerChars[2]);
		}
		else if (Input.GetButtonDown("j4Fire"))
		{
			playerHeroChoice[3] = 2;
			playerSelection[playerSpawn[3]].ChangeSelectedChar(playerChars[2]);
		}
		else if (Input.GetButtonDown("j5Fire"))
		{
			playerHeroChoice[4] = 2;
			playerSelection[playerSpawn[4]].ChangeSelectedChar(playerChars[2]);
		}
		
		
		if (Input.GetButtonDown("j1Ability1"))
		{
			playerHeroChoice[0] = 3;
			playerSelection[playerSpawn[0]].ChangeSelectedChar(playerChars[3]);
		}
		else if (Input.GetButtonDown("j2Ability1"))
		{
			playerHeroChoice[1] = 3;
			playerSelection[playerSpawn[1]].ChangeSelectedChar(playerChars[3]);
		}
		else if (Input.GetButtonDown("j3Ability1"))
		{
			playerHeroChoice[2] = 3;
			playerSelection[playerSpawn[2]].ChangeSelectedChar(playerChars[3]);
		}
		else if (Input.GetButtonDown("j4Ability1"))
		{
			playerHeroChoice[3] = 3;
			playerSelection[playerSpawn[3]].ChangeSelectedChar(playerChars[3]);
		}
		else if (Input.GetButtonDown("j5Ability1"))
		{
			playerHeroChoice[4] = 3;
			playerSelection[playerSpawn[4]].ChangeSelectedChar(playerChars[3]);
		}
	}

	IEnumerator StartCountdown()
	{
		float timer = 10f;
		countdownText.text = "" + Mathf.Ceil(timer) + "s to match";
		countdownText.gameObject.SetActive(true);
		yield return new WaitForFixedUpdate();

		while (timer >= 0f)
		{
			timer -= Time.fixedDeltaTime;
			countdownText.text = "" + Mathf.Ceil(timer) + "s to match";
			yield return new WaitForFixedUpdate();
		}
		countdownText.transform.SetPositionAndRotation(new Vector3(countdownText.transform.position.x,
			countdownText.transform.position.y-200f), Quaternion.identity);
		countdownText.text = "Match begins !";

		matchStarted = true;
		foreach (PlayerSelection ps in playerSelection)
		{
			ps.gameObject.SetActive(false);
		}
		
		for (int x = 0; x < 5; x++)
		{
			if (hasSpawned[x])
			{
				SpawnPlayer(x);
			}
		}

		StartCoroutine("ChangeAudio");
		
		yield return new WaitForSeconds(2f);
		countdownText.gameObject.SetActive(false);
	}

	void SpawnPlayer(int id)
	{
		GameObject go = Instantiate(players[playerHeroChoice[id]], 
			playerSpawnPositions[playerSpawn[id]].position,
			Quaternion.identity);
		PlayerController pc = go.GetComponent<PlayerController>();
		
		pc.playerId = playerSpawn[id];
		pc.controller = "j"+(id+1);
		pc.charAim.GetComponent<SpriteRenderer>().color = playersColor[playerSpawn[id]];
		pc.gameManager = this;

		PlayerUIController playerUiController = playersUI[playerSpawn[id]].GetComponent<PlayerUIController>();
		playerUiController.pc = pc;
		playerUiController.SetColor(playersColor[playerSpawn[id]]);
		playersUI[playerSpawn[id]].gameObject.SetActive(true);
		
		playerPos.Add(pc.transform);
//		Debug.Log("Player Id : " + id);
//		Debug.Log("Player Controller : " + pc.controller);
	}

	public void RemovePlayer(GameObject go, int id)
	{
		foreach (Transform pos in playerPos)
		{
			PlayerController pc = pos.GetComponent<PlayerController>();
			if (pc.playerId == id)
			{
				playersUI[id].gameObject.SetActive(false);
				playerPos.Remove(pos);
				go.SetActive(false);
				playerSpawnPoint--;
				break;
			}
		}

		if (playerSpawnPoint <= 1)
		{
			foreach (Transform pos in playerPos)
			{
				if (pos.gameObject.activeSelf)
				{
					PlayerController pc = pos.GetComponent<PlayerController>();
					playersUI[pc.playerId].gameObject.SetActive(false);
					countdownText.color = playersColor[pc.playerId];
					countdownText.text = "Player " + (pc.playerId+1) + " Wins !";
					countdownText.gameObject.SetActive(true);

					StartCoroutine("ReturnToMainMenu");
				}
			}
		}
	}

	IEnumerator SpawnPowerups()
	{
		while (true)
		{
			yield return new WaitForSeconds(powerupsSpawnDelay);
			
			if (activePowerups != null && !activePowerups.GetComponent<Powerup>().isPicked)
			{
				Destroy(activePowerups);
			}
			
			Transform puSpawnPos = powerupSpawnPositions[Random.Range(0, powerupSpawnPositions.Length)];
			GameObject pu = powerups[Random.Range(0, powerups.Length)];
			
			activePowerups = Instantiate(pu, puSpawnPos);
		}
	}

	IEnumerator ReturnToMainMenu()
	{
		yield return new WaitForSeconds(5f);

		SceneManager.LoadScene(0);
	}

	IEnumerator FadeInAudio()
	{
		float fadingTime = 0f;
		while (fadingTime <= 1f)
		{
			audioSource.volume = Mathf.Lerp(0f, 1f, fadingTime);
			yield return new WaitForSeconds(Time.deltaTime);
			fadingTime += Time.deltaTime;
		}
	}

	IEnumerator ChangeAudio()
	{
		float fadingTime = 0f;
		while (fadingTime <= 1f)
		{
			audioSource.volume = Mathf.Lerp(0f, 1f, 1f - fadingTime);
			yield return new WaitForSeconds(Time.deltaTime);
			fadingTime += Time.deltaTime;
		}

		audioSource.clip = audioClips[1];
		audioSource.Play();

		StartCoroutine("FadeInAudio");
	}
}
