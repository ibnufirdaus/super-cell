﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSelection : MonoBehaviour
{
    public Image pointer, selectedChar;

    public void ChangePointerColor(Color color)
    {
        pointer.color = color;
    }

    public void ChangeSelectedChar(Sprite sprite)
    {
        selectedChar.sprite = sprite;
    }
    
}
