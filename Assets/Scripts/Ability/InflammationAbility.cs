﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InflammationAbility : MonoBehaviour
{

	private WhiteBloodCell parentPC;
	[SerializeField] private float delay, duration;
	private bool burstMoment;
	[SerializeField] private float selfDamage, enemyDamage;
	private SpriteRenderer _spriteRenderer;
	public Sprite[] Sprites;
	private List<PlayerController> pcs;
	
	// Use this for initialization8'
	void Start () {
		parentPC = transform.parent.GetComponent<WhiteBloodCell>();
		_spriteRenderer = GetComponent<SpriteRenderer>();
		delay = 0f;
		duration = 2f;
		burstMoment = false;
		selfDamage = 75f;
		enemyDamage = 100f;
		pcs = new List<PlayerController>();
	}

	void Update()
	{
		if (delay >= duration && !burstMoment)
		{
			burstMoment = true;
			_spriteRenderer.sprite = Sprites[0];
			parentPC.Damage(selfDamage);
			foreach (var pc in pcs)
			{
				pc.Damage(enemyDamage);
			}
			Destroy(gameObject, 1f);
			
		}
		delay += Time.deltaTime;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			PlayerController pc = other.GetComponent<PlayerController>();
			if (pc.playerId != parentPC.playerId)
			{
				pcs.Add(pc);
			}
		}	
	}
}
