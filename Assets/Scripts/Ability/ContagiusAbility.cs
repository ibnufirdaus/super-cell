﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContagiusAbility : MonoBehaviour
{

	[SerializeField] private float shootDelay;
	private float prevShootDelay;
	private BacteriaCell parentPc;
	private float rotateLeft;
	
	// Use this for initialization
	void Start ()
	{
		parentPc = GetComponentInParent<BacteriaCell>();
//		Rotatable = true;
		rotateLeft = 360;
		prevShootDelay = parentPc.shootDelay;
		shootDelay = 0.125f;
		parentPc.rotateLocked = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (rotateLeft > 0)
		{
			parentPc.transform.Rotate(Vector3.back, Time.deltaTime * 90);
			parentPc.shootDelay = shootDelay;
		}
		if (rotateLeft <= 0)
		{
			parentPc.shootDelay = prevShootDelay;
			parentPc.rotateLocked = false;
			Destroy(gameObject);
		}
		rotateLeft -= Time.deltaTime * 90;
	}
}
