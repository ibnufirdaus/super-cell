﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConsumeAbility : MonoBehaviour
{

	private WhiteBloodCell ParentPc;
	[SerializeField] private float FirstDamage;
	[SerializeField] private float Dps;
	[SerializeField] private float Duration;
	private SpriteRenderer _spriteRenderer;
	private List<PlayerController> firstHitPlayers;
	private List<PlayerController> longHitPlayers;
	
	// Use this for initialization
	void Start ()
	{
		ParentPc = GetComponentInParent<WhiteBloodCell>();
		_spriteRenderer = GetComponent<SpriteRenderer>();
		firstHitPlayers = new List<PlayerController>();
		longHitPlayers = new List<PlayerController>();

		ParentPc.speed -= 20f;
		ParentPc.turnSpeed /= 2;
	}
	
	// Update is called once per frame
	void Update () {
		if (Duration <= 0)
		{
			ParentPc.speed += 20f;
			ParentPc.turnSpeed *= 2;
			Destroy(gameObject);
		}
		Duration -= Time.deltaTime;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			PlayerController pc = other.GetComponent<PlayerController>();
			if (pc.playerId != ParentPc.playerId)
			{
				ParentPc.Damage(-1 * FirstDamage); 
				pc.Damage(FirstDamage);	
			}
		}
	}

	private void OnTriggerStay2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			PlayerController pc = other.GetComponent<PlayerController>();
			if (pc.playerId != ParentPc.playerId)
			{
				ParentPc.Damage(-1 * FirstDamage * Time.deltaTime);
				pc.Damage(Dps * Time.deltaTime);	
			}
		}
	}
}
