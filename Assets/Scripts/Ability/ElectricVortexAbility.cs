﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElectricVortexAbility : MonoBehaviour
{
	private PlayerController parentPC;
	private float timer, expandRate;
	
	// Use this for initialization
	void Start () {
		Destroy(gameObject,5f);
		parentPC = transform.parent.GetComponent<PlayerController>();
		timer = 0f;
		expandRate= 3.5f;
	}
	
	// Update is called once per frame
	void Update()
	{
		if (timer > 2.5f)
		{
			transform.localScale -= new Vector3(expandRate * Time.deltaTime, expandRate * Time.deltaTime,
				expandRate * Time.deltaTime);
			float headingAngle = transform.rotation.eulerAngles.z + (180f * Time.deltaTime);
			Quaternion newHeading = Quaternion.Euler(0, 0, headingAngle);
			transform.rotation = Quaternion.RotateTowards(transform.rotation, newHeading, 10f);
			
		}
		else
		{
			transform.localScale += new Vector3(expandRate * Time.deltaTime, expandRate * Time.deltaTime, 
				expandRate * Time.deltaTime);
			float headingAngle = transform.rotation.eulerAngles.z + (180f * Time.deltaTime);
			Quaternion newHeading = Quaternion.Euler(0, 0, headingAngle);
			transform.rotation = Quaternion.RotateTowards(transform.rotation, newHeading, 10f);
			
		}

		timer += Time.deltaTime;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			PlayerController pc = other.GetComponent<PlayerController>();
			if (pc.playerId != parentPC.playerId)
			{
				pc.Damage(35f);
			}
		}
	}

	private void OnTriggerStay2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			PlayerController pc = other.GetComponent<PlayerController>();
			if (pc.playerId != parentPC.playerId)
			{
				pc.Damage(25f * Time.deltaTime);
			}
		}
	}
}
