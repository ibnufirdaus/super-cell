﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloodstrikeAbility : MonoBehaviour
{

	[SerializeField] private HashSet<PlayerController> pcs;
	[SerializeField] private float timer,threshold, stopTimer;

	private SpriteRenderer sr;
	
	// Use this for initialization
	void Start () {
		pcs = new HashSet<PlayerController>();
		timer = 0f;
		threshold = 2f;
		stopTimer = 1f;
		sr = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		timer += Time.deltaTime;
		sr.color = new Color(sr.color.r, sr.color.g, sr.color.b, 1 - (threshold-timer)/threshold);

		if (timer >= stopTimer)
		{
			transform.parent = null;
		}
		
		if (timer >= threshold)
		{
			Boom();
			Destroy(gameObject);
		}
	}
	
	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			PlayerController pc = other.GetComponent<PlayerController>();
			pcs.Add(pc);
			
		}
	}
	
	
	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			PlayerController pc = other.GetComponent<PlayerController>();
			pcs.Remove(pc);
			
		}
	}

	private void Boom()
	{
		foreach (PlayerController pc in pcs)
		{
			float staminaDmg = (100f - pc.stamina) * 0.20f;
			float totalDmg = 65f + staminaDmg;
			pc.Damage(totalDmg);
		}
	}
}
