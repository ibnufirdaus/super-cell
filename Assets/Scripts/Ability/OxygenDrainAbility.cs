﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OxygenDrainAbility : MonoBehaviour
{
	[SerializeField] private List<PlayerController> pcs;
	private PlayerController parentPC;
	
	// Use this for initialization
	void Start () {
		Destroy(gameObject,5);
		pcs = new List<PlayerController>();
		parentPC = transform.parent.GetComponent<PlayerController>();
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			PlayerController pc = other.GetComponent<PlayerController>();
			if (pc.playerId != parentPC.playerId)
			{
				pc.speed -= 5f;
				parentPC.speed += 2.5f;
				pcs.Add(pc);
			}

		}
	}

	private void OnTriggerStay2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			PlayerController pc = other.GetComponent<PlayerController>();
			if (pc.playerId != parentPC.playerId)
			{
				pc.stamina -= (75 * Time.deltaTime);
			}

		}
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			PlayerController pc = other.GetComponent<PlayerController>();
			if (pc.playerId != parentPC.playerId)
			{
				pc.speed += 5;
				parentPC.speed -= 2.5f;
				pcs.Remove(pc);
			}
		}
	}

	private void OnDestroy()
	{
		foreach (PlayerController pc in pcs)
		{
			parentPC.speed -= 2.5f;
			pc.speed += 5;
		}
	}
}
