﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUIController : MonoBehaviour
{

	public PlayerController pc;
	public Text playerId, playerHP, playerStamina;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		playerId.text = "" 
		    + Mathf.CeilToInt(pc.ability1CD) + " | "
			+ Mathf.CeilToInt(pc.ability2CD);
		playerHP.text = "HP : " + (int)pc.health;
		playerStamina.text = "Stm : " + (int)pc.stamina;
	}

	public void SetColor(Color color)
	{
		playerId.color = color;
		playerHP.color = color;
		playerStamina.color = color;
	}
}
