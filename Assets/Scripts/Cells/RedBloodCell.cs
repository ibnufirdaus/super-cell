﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedBloodCell : PlayerController
{

	public GameObject oxygenDrain, bloodStrike;
	
	public override void Start()
	{
		base.Start();
		
		maxHealth = 500f;
		maxStamina = 250f;
		
		health = maxHealth;
		stamina = maxStamina;
		staminaRegen = maxStamina/20f;
		
		turnSpeed = 30f;
		speed = 20f;
		shootDelay = 1.25f;
		dashedSpeed = 35f;

		ability1Delay = 10f;
		ability2Delay = 15f;
	}

	public override void Update()
	{
		base.Update();
	}

	public override void Ability1()
	{
		if (ability1CD <= 0f)
		{
			ability1CD = ability1Delay;
			base.Ability1();


			GameObject go = Instantiate(oxygenDrain, transform);
			go.transform.parent = transform;
		}
	}

	public override void Ability2()
	{
		if (ability2CD <= 0f)
		{
			ability2CD = ability2Delay;
			base.Ability2();

			List<Transform> targets = gameManager.playerPos;
			foreach (Transform target in targets)
			{
				if (target.GetComponent<PlayerController>().playerId != playerId)
				{
					Instantiate(bloodStrike, target.transform);
				}
			}
		}
	}
}
