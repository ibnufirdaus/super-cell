﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NeuronCell : PlayerController
{

	public GameObject memoryLapse, electricVortex;
	[SerializeField] private Transform memoryMark;
	[SerializeField] private bool memorized;

	// Use this for initialization
	public override void Start () {
		base.Start();
		
		maxHealth = 375f;
		maxStamina = 400f;
		
		health = maxHealth;
		stamina = maxStamina;
		staminaRegen = maxStamina/20f;
		
		turnSpeed = 30f;
		speed = 27.5f;
		shootDelay = 0.85f;
		dashedSpeed = 37.5f;

		ability1Delay = 10f;
		ability2Delay = 20f;

		memorized = false;
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update();
	}

	public override void Ability1()
	{
		if (!memorized && ability1CD <= 0f)
		{	
			ability1CD = ability1Delay;
			base.Ability1();
			GameObject go = Instantiate(memoryLapse, transform.position, Quaternion.identity) as GameObject;
			memoryMark = go.transform;
			memorized = true;
			StartCoroutine("MemoryLapsing");
		}
		else if (memorized)
		{
			StopCoroutine("MemoryLapsing");
			Memorize();
			memorized = false;
		}
	}

	public override void Ability2()
	{
		if (ability2CD <= 0f)
		{
			ability2CD = ability2Delay;
			base.Ability2();

			Instantiate(electricVortex, transform);
		}
	}

	IEnumerator MemoryLapsing()
	{
		yield return new WaitForSeconds(5f);
		Memorize();
		memorized = false;
	}

	void Memorize()
	{
		transform.position = memoryMark.position;
		Destroy(memoryMark.gameObject);
	}
}
