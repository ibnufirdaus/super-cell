﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BacteriaCell : PlayerController
{

	[SerializeField] private GameObject Contagious;
	public bool IsDoubleShoot;
	public Transform SplitAim1, SplitAim2, ShootPos1, ShootPos2;
	private Transform tempAim;
	private float duration;
	
	// Use this for initialization
	public override void Start () {
		base.Start();
		
		maxHealth = 300f;
		maxStamina = 200f;
		
		health = maxHealth;
		stamina = maxStamina;
		staminaRegen = maxStamina/20f;
		
		turnSpeed = 30f;
		speed = 27.5f;
		shootDelay = 0.55f;
		dashedSpeed = 37.5f;

		ability1Delay = 10f;
		ability2Delay = 15f;
		
		IsDoubleShoot = false;
		duration = 3f;
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update();
		
	}


	public override void Ability1()
	{
		if (ability1CD <= 0f)
		{
			ability1CD = ability1Delay;
			base.Ability1();
			Instantiate(Contagious, transform);
		}
	}

	public override void Ability2()
	{
		if (ability2CD <= 0f)
		{
			ability2CD = ability2Delay;
			base.Ability2();
			StartCoroutine("DoubleShoot");
		}
	}

	protected override void Shoot()
	{
		base.Shoot();
		if (IsDoubleShoot)
		{
			Vector2 direction = SplitAim2.position - ShootPos2.position;
			Rigidbody2D blt = Instantiate(bullet, SplitAim2.position, Quaternion.Euler(0, 0, transform.localEulerAngles.z))
				as Rigidbody2D;
			lastTimeShoot = Time.realtimeSinceStartup;
			blt.GetComponent<Bullet>().playerId = playerId;
			if (hasHeavyBullet)
			{
				blt.GetComponent<Bullet>().damage *= 2;
				blt.AddForce(direction * (bulletSpeed * 0.85f));
			}
			else
			{
				blt.AddForce(direction * bulletSpeed);
			}
		}
	}

	IEnumerator DoubleShoot()
	{
		
		tempAim = aim;
		aim = SplitAim1;
		shootPos = ShootPos1;
		IsDoubleShoot = true;
		
		yield return new WaitForSeconds(duration);
		
		aim = tempAim;
		tempAim = null;
		shootPos = transform;
		IsDoubleShoot = false;

	}
}
