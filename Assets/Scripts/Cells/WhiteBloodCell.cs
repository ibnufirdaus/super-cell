﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteBloodCell : PlayerController
{

	public GameObject Inflammation, Consume;
	
	// Use this for initialization
	public override void Start () {
		base.Start();
		
		maxHealth = 275f;
		maxStamina = 250f;
		
		health = maxHealth;
		stamina = maxStamina;
		staminaRegen = maxStamina/20f;
		
		turnSpeed = 30f;
		speed = 35f;
		shootDelay = 0.5f;
		dashedSpeed = 40f;

		ability1Delay = 10f;
		ability2Delay = 15f;
		
	}
	
	// Update is called once per frame
	public override void Update () {
		base.Update();
	}

	public override void Ability1()
	{
		if (ability1CD <= 0f )
		{
			ability1CD = ability1Delay;
			Instantiate(Inflammation, transform);
		}
	}

	public override void Ability2()
	{
		if (ability2CD <= 0f)
		{
			ability2CD = ability2Delay;
			Instantiate(Consume, transform.position + (aim.position - transform.position) * 2.5f, transform.rotation,
				transform);
		}
	}
}
