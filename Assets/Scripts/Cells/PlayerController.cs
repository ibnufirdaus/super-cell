﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.Design;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
	const float ROTATION_DEADZONE = 0.05f;

	public Rigidbody2D bullet;
	public Transform aim, shootPos;
	public GameObject charAim;
	public int playerId, bulletBlockCount, heavyBulletCount;
	public string controller;
	public GameManager gameManager;
	public float maxHealth, maxStamina;
	public float speed, dashedSpeed, turnSpeed, bulletSpeed, health, stamina, staminaRegen; 
	public float lastTimeShoot, shootDelay, ability1Delay, ability2Delay, ability1CD, ability2CD;
	public bool dashLocked, moveLocked, rotateLocked, shootLocked, burstStamina, hasHeavyBullet;
	Rigidbody2D rb2d;
	private AudioSource audioSource;
	
	// Use this for initialization
	public virtual void Start () {
		rb2d = GetComponent<Rigidbody2D>();
		shootPos = transform;
		
/** PERSONAL STAT - PLEASE ASSIGN IN EACH DERIVED CLASS
		health = 100f;
		stamina = 100f;
		staminaRegen = 7.5f;
		
		turnSpeed = 15f;
		dashMultiplier = 3f;
		speed = 11f;
		shootDelay = 0.25f;
**/
		
		bulletSpeed = 1500f;
		lastTimeShoot = Time.realtimeSinceStartup;
		ability1CD = 0f;
		ability2CD = 0f;

		dashLocked = false;
		GetComponent<Rigidbody2D>().sleepMode = RigidbodySleepMode2D.NeverSleep;

		burstStamina = false;
		hasHeavyBullet = false;
		bulletBlockCount = 0;
		heavyBulletCount = 0;

		audioSource = GetComponent<AudioSource>();
		gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
	}
	
	// Update is called once per frame
	public virtual void Update () {
		float moveHorizontal = Input.GetAxis(controller + "Horizontal");
		float moveVertical = Input.GetAxis(controller + "Vertical");
		if (Mathf.Abs(moveHorizontal) >= ROTATION_DEADZONE || Mathf.Abs(moveVertical) >= ROTATION_DEADZONE)
		{
			if (!rotateLocked)
			{
				Rotate(moveHorizontal, moveVertical);
			}
			
			if (!moveLocked)
			{
				Move(moveHorizontal, moveVertical);
			}
		}
		else
		{
			if (!dashLocked)
			{
				rb2d.velocity = Vector2.zero;
			}
		}

		if(Input.GetButton(controller + "Fire") && (Time.realtimeSinceStartup - lastTimeShoot) >= shootDelay) {
			if (!shootLocked)
			{
				lastTimeShoot = Time.realtimeSinceStartup;
				Shoot();
			}
		}

		if (Input.GetButtonDown(controller + "Dash") && !dashLocked)
		{
			if (burstStamina)
			{
				StartCoroutine("Dash");
			}
			else if (stamina >= 50f)
			{
				stamina -= 50f;
				StartCoroutine("Dash");
			}
		}

		if (Input.GetButtonDown(controller + "Ability1"))
		{
			Ability1();
		}
		
		if (Input.GetButtonDown(controller + "Ability2"))
		{
			Ability2();
		}

		stamina = Mathf.Clamp(stamina + (staminaRegen * Time.deltaTime),0f,maxStamina);
		ability1CD = Mathf.Clamp(ability1CD - Time.deltaTime, 0f, ability1Delay);
		ability2CD = Mathf.Clamp(ability2CD - Time.deltaTime, 0f, ability2Delay);
	}

	void Rotate(float hor, float ver)
	{
		// Rotating
		float headingAngle = Mathf.Atan2(ver, hor) * Mathf.Rad2Deg;
		Quaternion newHeading = Quaternion.Euler(0, 0, headingAngle);
		transform.rotation = Quaternion.RotateTowards(transform.rotation, newHeading, turnSpeed);
	}

	void Move(float hor, float ver)
	{
		// Moving
		if (!dashLocked)
		{
			Vector2 newPos = new Vector2(speed * hor, speed * ver);
			rb2d.velocity = newPos;
		}
	}

	protected virtual void Shoot()
	{
		if (heavyBulletCount <= 0)
		{
			hasHeavyBullet = false;
		}
		
		audioSource.Play();

		Vector2 direction = aim.position - shootPos.position;
		Rigidbody2D blt = Instantiate(bullet, aim.position, Quaternion.Euler(0, 0, transform.localEulerAngles.z))
			as Rigidbody2D;
		blt.GetComponent<Bullet>().playerId = playerId;
		if (hasHeavyBullet)
		{
			blt.GetComponent<Bullet>().damage *= 2;
			blt.AddForce(direction * (bulletSpeed * 0.85f));
			heavyBulletCount--;
		}
		else
		{
			blt.AddForce(direction * bulletSpeed);
		}
	}

	public void Damage(float damage)
	{
		health = Mathf.Clamp(health - damage, 0f, maxHealth);
		if (health == 0)
		{
			gameManager.RemovePlayer(gameObject, playerId);
		}
	}

	IEnumerator Dash()
	{
		float dirX = aim.position.x - transform.position.x;
		float dirY = aim.position.y - transform.position.y;
		
		dashLocked = true;
		rb2d.velocity = new Vector2(dirX * dashedSpeed, dirY * dashedSpeed);
		yield return new WaitForSeconds(.075f);
		dashLocked = false;
	}

	public virtual void Ability1()
	{
	}
	
	public virtual void Ability2()
	{
	}
}
