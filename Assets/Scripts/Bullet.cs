﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

	public int playerId;
	public float damage;

	// Use this for initialization
	void Start () {
		Destroy(gameObject,3f);
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player")
		{
			PlayerController otherPC = other.GetComponent<PlayerController>();
			int otherId = otherPC.playerId;
			if (playerId != otherId)
			{
				if (otherPC.bulletBlockCount > 0)
				{
					otherPC.bulletBlockCount--;
				}
				else
				{
					otherPC.Damage(damage);
				}
				Destroy(gameObject);
			}
		}
		else if (other.tag == "Obstacle")
		{
			Destroy(gameObject);
		}	
	}
}
